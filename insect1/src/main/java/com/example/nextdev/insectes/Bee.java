package com.example.nextdev.insectes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.libinsects.controller.BeeManager;
import com.example.libinsects.interfaces.UpdateUIInterface;
import com.example.libinsects.utils.Utils;
import com.example.libinsects.view.LifecycleLayout;
import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.List;

public class Bee extends AppCompatActivity implements UpdateUIInterface {

    private final static String TAG = Bee.class.getSimpleName();

    // Views
    private Button mSpeedButton, mFeedButton, mTemperatureButton;
    private TextView mStateTextView, mTemperatureTextView, mFeedTextView;
    private LifecycleLayout mLifecycleLayout;

    private BeeManager mBeeManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bee);
        SugarContext.init(getApplicationContext());
        initViews();
        mBeeManager = new BeeManager(this);
        mBeeManager.start();
        mLifecycleLayout.setInitialState(mBeeManager.getCurrentState());


        // Print db
        List<com.example.libinsects.model.Bee> bees =
                SugarRecord.listAll(com.example.libinsects.model.Bee.class);

        Log.e(TAG, bees.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBeeManager.stop();
    }

    private void initViews() {

        mStateTextView = (TextView) findViewById(R.id.stateTextView);
        mTemperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        mFeedTextView = (TextView) findViewById(R.id.feedTextView);
        mSpeedButton = (Button) findViewById(R.id.speedButton);
        mFeedButton = (Button) findViewById(R.id.feedButton);
        mTemperatureButton = (Button) findViewById(R.id.temperatureButton);
        mLifecycleLayout = (LifecycleLayout) findViewById(R.id.beeLayout);

        // Init button click listeners
        mSpeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBeeManager.speedUp();
            }
        });

        mFeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean hasFeed = mBeeManager.feed();
                mFeedTextView.setText(String.format("Feed: %s", hasFeed));
            }
        });

        mTemperatureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentTemperature = mBeeManager.generateTemperature();
                mTemperatureTextView.setText(String.format("Temperature: %d", currentTemperature));
            }
        });

    }


    @Override
    public void UpdateUI(com.example.libinsects.model.Bee bee) {
        mStateTextView.setText(String.format("State: %s", bee.getCurrentState().getStateName()));
        mTemperatureTextView.setText(String.format("Temperature: %d", bee.getCurrentTemperature()));
        mFeedTextView.setText(String.format("Feed: %s", bee.hasFed()));
        mLifecycleLayout.next(bee.getCurrentState().getStateName(), bee.shouldKillBee());

        if (bee.shouldKillBee()) {
            String messageFormat = String.format("Bye Bye Bee %d :(", bee.getId());
            Utils.showToast(Bee.this, messageFormat);
        }
    }
}
