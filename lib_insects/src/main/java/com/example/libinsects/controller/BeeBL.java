package com.example.libinsects.controller;

import android.util.Log;

import com.example.libinsects.model.Bee;
import com.example.libinsects.model.states.Adult;
import com.example.libinsects.model.states.Egg;
import com.example.libinsects.model.states.Larva;
import com.example.libinsects.model.states.Pupa;
import com.example.libinsects.model.states.State;
import com.orm.SugarRecord;


/**
 * Created by orensharon on 3/3/17.
 */

public class BeeBL {

    private final String TAG = getClass().getSimpleName();

    private Bee mBee;


    public void next() {

        if (mBee == null) {
            init();
            return;
        }

        if (shouldKillBee()) {
            lay();
            return;
        }

        Log.e(TAG, "next(state=" + mBee.getCurrentState().getStateName() + ") + shouldKillBee()=" + shouldKillBee());

        // Bee state machine, entry point after each lifecyle stage
        switch (mBee.getCurrentState().getStateName()) {

            case State.EGG:
                mBee.setState(new Larva());
                break;
            case State.LARVA:
                mBee.setState(new Pupa());
                break;
            case State.PUPA:
                mBee.setState(new Adult());
                break;
            case State.ADULT:
                lay();
                break;
        }

    }

    public void init() {
        // Try to find the latest record in db
        // If exist - retrieve state and proceed, otherwise start
        // from initial state
        Bee savedBee = SugarRecord.last(Bee.class);
        if (savedBee != null) {
            mBee = savedBee;
            mBee.recoverStateObject();
        } else {
            mBee = new Bee();
            lay();
        }
    }

    /**
     * Create a new egg
     */
    private void lay() {
        mBee.setState(new Egg());
    }


    private boolean shouldKillBee() {
        return mBee.shouldKillBee();
    }

    public int generateTemperature() {
        return mBee.generateTemperature();
    }

    public boolean feed() {
        return mBee.feed();
    }

    public Bee getBee() {
        return mBee;
    }

}
