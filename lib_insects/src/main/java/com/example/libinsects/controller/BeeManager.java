package com.example.libinsects.controller;

import com.example.libinsects.interfaces.BeeInterface;
import com.example.libinsects.interfaces.UpdateUIInterface;
import com.example.libinsects.model.Bee;
import com.example.libinsects.model.states.State;
import com.example.libinsects.utils.Constants;
import com.orm.SugarRecord;
import android.os.Handler;


/**
 * Created by orensharon on 3/3/17.
 */

public class BeeManager implements BeeInterface {


    private final String TAG = getClass().getSimpleName();

    private float mSpeedFactor = 1;

    private Handler mHandler;
    private Runnable mRunnable;
    private UpdateUIInterface mUpdateUICallback;

    // Bee business logic
    private final BeeBL mBeeBL;

    public BeeManager(UpdateUIInterface updateUICallback) {
        mUpdateUICallback = updateUICallback;
        mBeeBL = new BeeBL();
    }

    public void start() {
        // Set repeated task
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                cycle();
            }
        };
        mRunnable.run();
    }

    public void stop() {
        mHandler.removeCallbacks(mRunnable);
    }

    /**
     * Perform cycle in bee's life
     */
    private void cycle() {
        mBeeBL.next();
        updateUI();
        cache();
        long cycleSpeed = generateCycleSpeed();
        mHandler.postDelayed(mRunnable, cycleSpeed);
    }




    @Override
    public int generateTemperature() {
        return mBeeBL.generateTemperature();
    }

    @Override
    public boolean feed() {
        return mBeeBL.feed();
    }

    @Override
    public void speedUp() {
        mSpeedFactor = mSpeedFactor * 0.5f;
    }


    public String getCurrentState() {
        return mBeeBL.getBee().getCurrentState().getStateName();
    }

    private long generateCycleSpeed() {
        long cycleSpeed = (long) (Constants.CYCLE_DEFAULT_DURATION * mSpeedFactor);
        cycleSpeed = cycleSpeed < Constants.CYCLE_MINIMUM_DURATION? Constants.CYCLE_MINIMUM_DURATION: cycleSpeed;
        return cycleSpeed;
    }

    /**
     * Save current bee state to database
     */
    private void cache() {
        // Copying the object before inserting to database
        // To prevent update instead of creation
        Bee bee = mBeeBL.getBee();
        if (bee != null) {
            Bee copied = new Bee(bee);
            SugarRecord.save(copied);
        }
    }


    private void updateUI() {
        if (mUpdateUICallback != null) {
            mUpdateUICallback.UpdateUI(mBeeBL.getBee());
        }
    }

}
