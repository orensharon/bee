package com.example.libinsects.interfaces;

/**
 * Created by orensharon on 3/5/17.
 */

public interface BeeInterface {

    int generateTemperature();
    boolean feed();
    void speedUp();
}
