package com.example.libinsects.interfaces;

import com.example.libinsects.model.Bee;

/**
 * Created by nextdev on 3/2/17.
 */

public interface UpdateUIInterface {
    void UpdateUI(Bee bee);
}
