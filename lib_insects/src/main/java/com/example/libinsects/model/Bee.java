package com.example.libinsects.model;

import android.util.Log;

import com.example.libinsects.model.states.Adult;
import com.example.libinsects.model.states.Egg;
import com.example.libinsects.model.states.Larva;
import com.example.libinsects.model.states.Pupa;
import com.example.libinsects.model.states.State;
import com.example.libinsects.utils.Utils;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;


/**
 * Created by nextdev on 3/2/17.
 */
@Table
public class Bee {

    /**
     * Properties
     */
    private Long id;
    private int currentTemperature;
    private boolean hasFeed;
    private long timestamp;

    @Column(name="currentState")
    private String stateName;

    @Ignore
    private State currentState;



    public Bee() {}

    public Bee(Bee bee) {
        this.currentTemperature = bee.getCurrentTemperature();
        this.hasFeed = bee.hasFeed;
        this.timestamp = bee.timestamp;
        this.currentState = new State(bee.currentState);
        this.stateName = currentState.getStateName();
    }

    public void setState(State state) {
        currentState = state;
        stateName = currentState.getStateName();
        timestamp = System.currentTimeMillis();

        // Each change in state (i.e new state in lifecycle)
        // also inits feed status and temperature
        hasFeed = true;
        currentTemperature = currentState.getSafeTemperature();

        Log.e(this.getClass().getSimpleName(), "setState(" + state + ") temp=" + currentTemperature + " hasFed=" + hasFeed);
    }

    public int generateTemperature() {
        currentTemperature = Utils.randomTemperature();
        Log.e(this.getClass().getSimpleName(), "generateTemperature(" + currentTemperature + ")");
        return currentTemperature;
    }

    /**
     * Feed or un-feed bee
     * @return if bee should be feed
     */
    public boolean feed() {
        hasFeed = !hasFeed;
        Log.e(this.getClass().getSimpleName(), "feed(" + hasFeed + ")");
        return hasFeed;
    }


    /**
     * Method to determine if bee should be kill in this lifecycle
     * during bad temperature or if bee didn't feed
     * @return true if bee should be killed
     */
    public boolean shouldKillBee() {
        return !hasValidTemperature() || !hasFeed;
    }

    public Long getId() {
        return id;
    }

    public int getCurrentTemperature() {
        return currentTemperature;
    }

    public boolean hasFed() {
        return hasFeed;
    }

    public State getCurrentState() {
        return currentState;
    }


    /**
     * A work around to recover bee state object after
     * bee has retrieved from database history
     */
    public void recoverStateObject() {
        switch (stateName) {
            case State.EGG:
                currentState = new Egg();
                break;
            case State.LARVA:
                currentState = new Larva();
                break;
            case State.PUPA:
                currentState = new Pupa();
                break;
            case State.ADULT:
                currentState = new Adult();
                break;
        }
    }

    private boolean hasValidTemperature() {
        return currentTemperature > currentState.getMinTemperature() &&
                currentTemperature < currentState.getMaxTemperature();
    }

    @Override
    public String toString() {
        return "Bee{" +
                "id='" + id+ '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", currentState='" + currentState + '\'' +
                ", stateName='" + stateName + '\'' +
                ", currentTemperature=" + currentTemperature +
                ", hasFeed=" + hasFeed +
                '}';
    }

}
