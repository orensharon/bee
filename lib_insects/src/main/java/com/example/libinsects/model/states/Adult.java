package com.example.libinsects.model.states;

/**
 * Created by orensharon on 3/3/17.
 */

public class Adult extends State {

    public static final int MIN_TEMPERATURE = 8;
    public static final int MAX_TEMPERATURE = 28;

    public Adult() {
        super(ADULT, MIN_TEMPERATURE, MAX_TEMPERATURE);
    }


}
