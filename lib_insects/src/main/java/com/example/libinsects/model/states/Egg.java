package com.example.libinsects.model.states;

/**
 * Created by orensharon on 3/3/17.
 */

public class Egg extends State {

    public static final int MIN_TEMPERATURE = 10;
    public static final int MAX_TEMPERATURE = 26;

    public Egg() {
        super(EGG, MIN_TEMPERATURE, MAX_TEMPERATURE);
    }


}
