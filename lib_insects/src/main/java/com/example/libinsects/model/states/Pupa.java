package com.example.libinsects.model.states;

/**
 * Created by orensharon on 3/3/17.
 */

public class Pupa extends State {

    public static final int MIN_TEMPERATURE = 12;
    public static final int MAX_TEMPERATURE = 28;

    public Pupa() {
        super(PUPA, MIN_TEMPERATURE, MAX_TEMPERATURE);
    }


}
