package com.example.libinsects.model.states;

/**
 * Created by orensharon on 3/3/17.
 */

public class State {

    public static final String EGG = "EGG";
    public static final String LARVA = "LARVA";
    public static final String PUPA = "PUPA";
    public static final String ADULT = "ADULT";

    private String state;
    private int maxTemperature, minTemperature;

    public State(String state, int minTemperature, int maxTemperature) {
        this.state = state;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    public State(State oldState) {
        state = oldState.getStateName();
        maxTemperature = oldState.getMaxTemperature();
        minTemperature = oldState.getMinTemperature();
    }

    public String getStateName() {
        return state;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public int getMinTemperature() {
        return minTemperature;
    }

    public int getSafeTemperature() {
        return (maxTemperature + minTemperature) / 2;
    }

    @Override
    public String toString() {
        return "State{" +
                "state='" + state + '\'' +
                ", maxTemperature=" + maxTemperature +
                ", minTemperature=" + minTemperature +
                '}';
    }

}
