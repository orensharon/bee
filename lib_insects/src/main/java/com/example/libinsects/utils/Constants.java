package com.example.libinsects.utils;

/**
 * Created by orensharon on 3/3/17.
 */

public class Constants {

    public final static long CYCLE_DEFAULT_DURATION = 5000L;
    public final static long CYCLE_MINIMUM_DURATION = 3000L;

    public final static long ANIMATION_DURATION = 2000L;

    public final static int MINIMUM_TEMPERATURE = 0;
    public final static int MAXIMUM_TEMPERATURE = 30;

}
