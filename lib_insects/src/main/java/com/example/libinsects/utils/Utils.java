package com.example.libinsects.utils;

import android.content.Context;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by orensharon on 3/3/17.
 */

public class Utils {

    /**
     * Generates an random temperature from 0 - MAXIMUM_TEMPERATURE
     */
    public static int randomTemperature() {
        Random r = new Random();
        return r.nextInt(Constants.MAXIMUM_TEMPERATURE);
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }


}
