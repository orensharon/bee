package com.example.libinsects.view;

import android.view.animation.Interpolator;

/**
 * Created by orensharon on 3/5/17.
 */

public class CrawlingInterpolator implements Interpolator {

    public CrawlingInterpolator() {}

    public float getInterpolation(float t) {
        float x= 2.0f * t - 1.0f;
        return 0.5f * (x * x * x + 1.0f);
    }
}
