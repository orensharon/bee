package com.example.libinsects.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.example.libinsects.model.states.State;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.example.libinsects.utils.Utils.getDensity;

/**
 * Created by orensharon on 3/4/17.
 */

public class LifecycleLayout extends FrameLayout {

    private  final String TAG = getClass().getSimpleName();

    private final int VIEW_WIDTH = (int) (200 * getDensity(getContext()));
    private final int VIEW_HEIGHT = (int) (200 * getDensity(getContext()));

    private LifecycleView mView;
    private Map<String, Float[]> stateToCoords = new LinkedHashMap<>();


    public LifecycleLayout(Context context) {
        this(context, null);
    }

    public LifecycleLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LifecycleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addBeeView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LifecycleLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        addBeeView();
    }

    private void addBeeView() {
        mView = new LifecycleView(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                VIEW_WIDTH,
                VIEW_HEIGHT
        );
        mView.setLayoutParams(layoutParams);
        addView(mView);

    }

    /**
     * {targetX, targetY}
     */
    private void mapStatesLocations() {
        // IMPORTANT TO PRESERVE ORDER!!
        stateToCoords.put(State.EGG, new Float[] { getWidth() * 0.5f - VIEW_WIDTH * 0.5f , 0.0f });
        stateToCoords.put(State.LARVA, new Float[] { getWidth() * 0.5f - VIEW_WIDTH * 0.5f, Float.valueOf(getHeight() - VIEW_HEIGHT )});
        stateToCoords.put(State.PUPA, new Float[] { getWidth() * 0.5f - VIEW_WIDTH * 0.5f, Float.valueOf(getHeight() - VIEW_HEIGHT )});
        stateToCoords.put(State.ADULT, new Float[] { getWidth() * 0.5f - VIEW_WIDTH * 0.5f, 0.0f });
    }


    public void next(final String state, boolean shouldBeeKilled) {
        mView.animateToNextState(state, shouldBeeKilled, stateToCoords.get(state));
    }

    public void setInitialState(final String initialState) {
        // Move view object to initial state position
        // and create map of all state location by states
        ViewTreeObserver vto = mView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mapStatesLocations();

                if (initialState != null) {
                    mView.setX(stateToCoords.get(initialState)[LifecycleView.TARGET_X]);
                    mView.setY(stateToCoords.get(initialState)[LifecycleView.TARGET_Y]);
                }

                if (Build.VERSION.SDK_INT < 16) {
                    mView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    mView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });
    }

}
