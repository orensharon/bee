package com.example.libinsects.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.example.libinsects.R;
import com.example.libinsects.model.states.State;
import com.example.libinsects.utils.Constants;


/**
 * Created by orensharon on 3/4/17.
 */

public class LifecycleView extends View {

    private  final String TAG = getClass().getSimpleName();

    public static final int TARGET_X = 0;
    public static final int TARGET_Y = 1;

    private boolean mHasKilled = false;

    // Animation objects
    private AnimatorSet mAnimationSet;
    private ObjectAnimator mObjectAnimatorX, mObjectAnimatorY;
    private final CrawlingInterpolator mCrawlingInterpolator = new CrawlingInterpolator();
    private final AccelerateInterpolator mAccelerateInterpolator = new AccelerateInterpolator();

    public LifecycleView(Context context) {
        this(context, null);
    }

    public LifecycleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LifecycleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAnimators();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LifecycleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAnimators();
    }


    private void initAnimators() {

        mObjectAnimatorX = ObjectAnimator.ofFloat(this, View.X , 0.0f);
        mObjectAnimatorY = ObjectAnimator.ofFloat(this, View.Y , 0.0f);

        mAnimationSet = new AnimatorSet();
        mAnimationSet.playTogether(mObjectAnimatorX, mObjectAnimatorY);
        mAnimationSet.setDuration(Constants.ANIMATION_DURATION);

    }



    public void animateToNextState(String state, boolean shouldBeeKilled, Float... values) {

        setAlpha(1.0f);

        // Set background resource by state
        int resource = getBackgroundResourceByState(state);
        setBackgroundResource(resource);

        // Kill bee and handle animation
        if (handleKillBe(shouldBeeKilled, values)) {
            return;
        }

        animateNextState(state, values);

    }

    private void animateNextState(String state, Float[] values) {
        // Animate to goNext state
        if (values != null && values.length == 2) {
            mObjectAnimatorX.setFloatValues(values[TARGET_X]);
            mObjectAnimatorY.setFloatValues(values[TARGET_Y]);

            if (state.equals(State.LARVA)) {
                mAnimationSet.setInterpolator(mCrawlingInterpolator);
            } else {
                mAnimationSet.setInterpolator(mAccelerateInterpolator);
            }

            mAnimationSet.start();
        }
    }

    /**
     * // Bee killed in previous state - move it to goNext position
     */
    private boolean handleKillBe(boolean shouldBeeKilled, Float[] values) {
        if (mHasKilled) {
            setX(values[TARGET_X]);
            setY(values[TARGET_Y]);
            mHasKilled = false;
            return true;
        }

        // Save killed flag to goNext state animation
        mHasKilled = shouldBeeKilled;

        // Kill bee with alpha animation
        if (shouldBeeKilled) {
            animate().alpha(0.0f).setDuration(Constants.ANIMATION_DURATION).start();
            return true;
        }
        return false;
    }

    private int getBackgroundResourceByState(String state) {
        switch (state) {
            case State.EGG:
                return R.drawable.egg;
            case State.LARVA:
                return R.drawable.larva;
            case State.PUPA:
                return R.drawable.pupa;
            case State.ADULT:
                return R.drawable.adult;
        }

        return 0;
    }
}
